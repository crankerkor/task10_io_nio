package com.oleksandr.nio.client_server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class Client {

    public static void main(String[] args) throws IOException, InterruptedException {
        InetSocketAddress address = new InetSocketAddress("localhost", 1111);
        SocketChannel client = SocketChannel.open(address);

        System.out.println("Connecting Server on port 8000");

        ArrayList<String> companies = new ArrayList<>();

        companies.add("Lil");
        companies.add("Peep");
        companies.add("Crusty");

        for (String company : companies) {

            byte[] message = (company).getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(message);
            client.write(buffer);

            System.out.println("sending " + company);
            buffer.clear();

            Thread.sleep(2000);
        }
        client.close();
    }
}

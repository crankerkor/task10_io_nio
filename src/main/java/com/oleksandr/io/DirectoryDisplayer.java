package com.oleksandr.io;

import java.io.File;

public class DirectoryDisplayer {

    public void showDirectory(String path) {
    File dir = new File(path);
        StringBuilder result = new StringBuilder();

        if (dir.isDirectory()) {
            File[] files = dir.listFiles();

            for (File file : files) {
                result.append(file.getName());
                if (file.isDirectory()) {
                    result.append(" dir ");
                } else if (file.isFile()) {
                    result.append(" file ");
                }

                result.append(getAttributes(file));
                result.append("\n");
            }

        }

        System.out.println(result);

    }

    private StringBuilder getAttributes(File file) {
        StringBuilder attributes = new StringBuilder();

        attributes.append(file.canExecute() ? " exec " : " not exec ");

        attributes.append(file.canRead() ? " read " : " not read ");

        attributes.append(file.canWrite() ? " write " : " not write ");

        attributes.append(file.isHidden() ? " hidden " : " not hidden ");

        return attributes;

    }
}

package com.oleksandr.io;

import java.io.*;

public class SourceCodeReader {
    private StreamTokenizer tokenizer;

    public void showComments(String filepath) {
    try (InputStream inputStream =
            new FileInputStream(filepath);
        Reader bufferedReader =
                new BufferedReader(new InputStreamReader(inputStream))) {

        tokenizer = new StreamTokenizer(
                bufferedReader
        );
        tokenizer.slashSlashComments(false);
        tokenizer.slashStarComments(false);
        tokenizer.wordChars('/', '/');
        tokenizer.wordChars('*', '*');
        tokenizer.eolIsSignificant(true);

        int ttype = 0;
        int lastline = -1;
        String s = "";

        while (ttype != StreamTokenizer.TT_EOF) {
            ttype = tokenizer.nextToken();
            int lineNum = tokenizer.lineno();

            String sValue = ttype == StreamTokenizer.TT_WORD ? tokenizer.sval : "";

            if (lineNum == lastline) {
                if (!sValue.startsWith("/")) {
                    lastline = lineNum;
                    continue;
              } else {
                    if (sValue.startsWith("/*")) {
                        do {
                            s += " " + sValue;
                            ttype = tokenizer.nextToken();
                            sValue = ttype == StreamTokenizer.TT_WORD ? tokenizer.sval : "";
                        } while (!sValue.startsWith("*/"));
                        s += sValue;

                    } else {
                      while (tokenizer.ttype != StreamTokenizer.TT_EOL) {
                        s += " " + sValue;
                        ttype = tokenizer.nextToken();
                        sValue = ttype == StreamTokenizer.TT_WORD ? tokenizer.sval : "";
                      }
                            }
                }
            } else {
                if (lastline != - 1) {
                    if (s.contains("/")) {
                      System.out.println(lastline + "\t" + s);
                      s = "";
                     }
                }
            }
            lastline = lineNum;
        }



        }catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

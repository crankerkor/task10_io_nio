package com.oleksandr.io;

import java.io.Serializable;

public class RepairingDroid extends Droid implements Serializable {
    private int repairingPower;
    private String instruments;

    public RepairingDroid(int armor, String model, int height,
                          int repairingPower, String instruments) {
        super(armor, model, height);
        this.instruments = instruments;
        this.repairingPower = repairingPower;
    }

    public void repair() {
        System.out.println("I am " + super.model + " and repairing with " + repairingPower +
                " repairing skills and using these instruments: " + instruments);
    }
}

package com.oleksandr.io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ship {
    private List<? extends Droid> droids;

    public Ship() {
        droids = createDroids();
    }

    public void writeDroidsToFile() {
        try (
      FileOutputStream fos =
          new FileOutputStream(
              "C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\src\\main\\java\\com\\oleksandr\\io\\ship\\Droids.ser");
            ObjectOutputStream out = new ObjectOutputStream(fos)
        ) {
            for (Droid droid : droids) {
                System.out.println("Writing " + droid.model + " to file");
                out.writeObject(droid);

            }

        } catch (FileNotFoundException ex) {
            System.out.println("File not found in writing");
        } catch (IOException ex) {
            System.out.println("I/O exception");
        }
    }

    public List<Droid> readDroidsFromFile() {
        List<Droid> droidsFromFile = new ArrayList<>();

        try (
      FileInputStream fis =
          new FileInputStream(
              "C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\src\\main\\java\\com\\oleksandr\\io\\ship\\Droids.ser");

        ObjectInputStream in = new ObjectInputStream(fis);
        ) {
            BattleDroid B2 = (BattleDroid) in.readObject();
            droidsFromFile.add(B2);
            System.out.println("Reading " + B2.model + " to file");

            RepairingDroid R2D2 = (RepairingDroid) in.readObject();
            droidsFromFile.add(R2D2);
            System.out.println("Reading " + R2D2.model + " to file");

        } catch (FileNotFoundException ex) {
            System.out.println("File not found in reading");
        } catch (IOException ex) {
            System.out.println("IO exception");
        } catch (ClassNotFoundException classEx) {
            System.out.println("Class not found");
        }

        return droidsFromFile;
    }

    public List<? extends Droid> createDroids() {
        List<Droid> droids = new ArrayList<>();

        BattleDroid B2 = new BattleDroid(
                500, "B2", 197, 300, 1);
        droids.add(B2);

        RepairingDroid R2D2 = new RepairingDroid(
                200, "R2D2", 125, 600, "pliers");
        droids.add(R2D2);

        return droids;
    }
}

package com.oleksandr.io;

import java.io.Serializable;

public class BattleDroid extends Droid implements Serializable {
    private int strength;
    private int amountOfGuns;

    public BattleDroid(int armor, String model, int height,
                       int strength, int amountOfGuns) {
        super(armor, model, height);
        this.amountOfGuns = amountOfGuns;
        this.strength = strength;
    }

    public void attack() {
        String gun = amountOfGuns > 1 ? "guns" : "gun";

        System.out.println("I am " + super.model + " and attacking with " + amountOfGuns +
              gun + " and " + strength + " strength");
    }
}

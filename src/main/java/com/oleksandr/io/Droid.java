package com.oleksandr.io;

import java.io.Serializable;

public class Droid implements Serializable {
    private transient int armor;
    protected String model;
    private transient int height;

    public Droid(int armor, String model, int height) {
        this.armor = armor;
        this.model = model;
        this.height = height;
    }
}

package com.oleksandr.io;

import java.io.*;

public class ReaderComparator {

    public byte[] readFromFile() {

        try (
        FileInputStream fos = new
                FileInputStream("C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\1");
        DataInputStream in = new DataInputStream(fos)
        ) {
        int count = in.available();

        byte[] buffer = new byte[count];

        in.read(buffer);

        for (byte b : buffer) {
            char c = (char) b;
        }

        return buffer;

        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("IO exception");
        }

        return new byte[0];
    }

    public String readFromFileWithBufferedReader() {

            try (
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream("C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\1")
                            )
                    )
            ) {
                StringBuilder builder = new StringBuilder();
                String st;

                while ((st = in.readLine()) != null) {
                    builder.append(st);
                }

                return builder.toString();

            } catch (FileNotFoundException ex) {
                System.out.println("File not found");
            } catch (IOException ex) {
                System.out.println("IO exception");
            }

            return "";
    }

    public void writeToFile() {

        try (
                FileOutputStream fos = new
                        FileOutputStream("C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\2.txt");
                DataOutputStream out = new DataOutputStream(fos)
        ) {

            byte[] buffer = readFromFile();

            out.write(buffer);


        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("IO exception");
        }
    }

    public void writeToFileWithBufferedWriter() {
        try (
                BufferedWriter out = new BufferedWriter(
                        new OutputStreamWriter(
                                new FileOutputStream("C:\\Users\\sashk\\Desktop\\string\\task10_io_nio\\2")
                        )
                )
        ) {
            String toWrite = readFromFileWithBufferedReader();

            out.write(toWrite);

        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("IO exception");
        }
    }
}

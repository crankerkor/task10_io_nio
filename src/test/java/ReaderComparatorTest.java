import com.oleksandr.io.ReaderComparator;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;


public class ReaderComparatorTest {
    private ReaderComparator comparator = new ReaderComparator();

    @Test
    public void readFromFileTest() {
        System.out.println("Reading");
        long startTime = System.nanoTime();

        comparator.readFromFile();

        long endTime = System.nanoTime();

        System.out.println(((endTime - startTime) / 1000000) + " milliseconds");

    }

    @Test
    public void readFromFileWithBufferedReader() {
        long startTime = System.nanoTime();

        comparator.readFromFileWithBufferedReader();

        long endTime = System.nanoTime();

        System.out.println(((endTime - startTime) / 1000000) + " milliseconds");
    }

    @Test
    public void writeToFileTest() {
        long startTime = System.nanoTime();

        comparator.writeToFile();

        long endTime = System.nanoTime();

        System.out.println((((endTime - startTime) / 1000000) - 200) + " milliseconds");
    }

    @Test
    public void writeToFileWithBufferedWriter() {
        System.out.println("Writing");
        long startTime = System.nanoTime();

        comparator.writeToFileWithBufferedWriter();

        long endTime = System.nanoTime();

        System.out.println((((endTime - startTime) / 1000000) - 5500) + " milliseconds");
    }


}
